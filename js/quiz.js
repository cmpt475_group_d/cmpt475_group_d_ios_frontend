/*
 created by Terence (Jienan) Chen from group B, Team #1
 created: 2014.07.01
 updated: 2014.07.20
 updated: 2014.07.22
 updated: 2014.07.23
 updated: 2014.07.24
 updated: 2014.07.25
 updated: 2014.07.26
 */


//definition of variables
//*
//var port_location = 'http://127.0.0.1:8000/';
var port_location = 'http://candyman.biz/';
var key = '';
var student_id = localStorage.getItem('id')

//define a current quiz
var currentQuiz = {
    id : null
};

//define an instance of quiz
var quizInfo = {
    id : null,
    result : null
};

//define an instance of question
var questionInfo = {
    id : null,
    result : null
};

//recursively load all possible answers
var possibleAnswerInfo = {
    id : null,
    result : []
};

//set studentQuiz properties
var studentQuiz = {
    quiz : null,
    student : null,
    quizDueTime : null,
score: null,
completed: false
};

//define an instance of studentQuiz
var studentQuizInfo = {
    id : null,
    result : null
};

//set a studentAnswer properties
var studentAnswer = {
studentQuiz: null,
answerText: null
};

//initialize studentAnswer id for the purpose of updating student answer info
//student id will not change in each studentQuiz instance
var studentAnswer_1_id = null,
studentAnswer_2_id = null,
studentAnswer_3_id = null,
studentAnswer_4_id = null,
studentAnswer_5_id = null;

//initialize answer_flag for the purpose of updating student answer
//answer_flag perform as a switch
var answer_flag_1 = true,
answer_flag_2 = true,
answer_flag_3 = true,
answer_flag_4 = true,
answer_flag_5 = true;

//add flags for adding score
var score_increment_1 = false,
score_increment_2 = false,
score_increment_3 = false,
score_increment_4 = false,
score_increment_5 = false;

//flag for checking if quiz has been taken
var flag = false;

//boolean variable for rusuming the quiz
var resume = true;

//define global quiz due time
var due = null;

//prevent the loss of JQM css after page change
$(document).bind('pagechange', function() {
                 $('.ui-page-active .ui-listview').listview('refresh');
                 $('.ui-page-active :jqmData(role=content)').trigger('create');
                 });

//load a list of quizzes using ajax call
$(document).on('pageinit', '#quiz', function(){
               var url = 'api/group_b/quiz/',
               mode = '?format=json';
               
               $.ajax({
                      url: port_location + url + mode + key,
                      dataType: "json",
                      async: true,
                      success: function (data) {
                      console.log(data);
                      quiz_ajax.parseJSONP(data);
                      },
                      error: function (request,error) {
                      alert('Cannot get quizzes. Network error has occurred please try again!');
                      }
                      });
               });

//parse a list of quizzes JSON, and present them on a page
var quiz_ajax = {
parseJSONP:function(data){
    quizInfo.result = data;
    $.each(data, function(i, row) {
           //output the ojects in the array for testing purpose
           console.log(JSON.stringify(row));
           $('#quiz_list').append('<li><a href="" data-id="' + row.id +'"><h3>' + row.quizName + '</h3><p>Due: ' + row.dueDay + '</p></a></li>');
           });
    $('#quiz_list').listview('refresh');
}
}

//warning dialog box
//click cancel button
$(document).on('pagebeforeshow', '#popupDialog', function(){
               //click cancel button to resume the quiz
               $('#cancel_btn').dialog('close');
               });

//click submit button
$(document).on('vclick', '#submit_btn', function(){
               //click submit button to submit the quiz
               alert('Quiz submitted.');
               //reset possibleAnswer result to null for score calculation purpose
               possibleAnswerInfo.result = [];
               
               //reset flag to false to eliminate the conflict between quizzes
               flag = false;
               
               //set this studentQuiz to completed state
               studentQuiz.completed = true;
               
               //update studentQuiz with score
               $.ajax({
                      type: 'PUT',
                      url: port_location + 'api/group_b/studentQuiz/' + studentQuizInfo.id +'/' + key,
                      dataType: "json",
                      data: studentQuiz,
                      async: false,
                      success: function (data) {
                      console.log(data);
                      },
                      error: function (request,error) {
                      alert('Cannot update studentQuiz.');
                      }
                      })
               
               });

//rearrage the quiz information, and present them on a page
$(document).on('pagebeforeshow', '#quiz_detail', function(){
               $('#quiz_data').empty();
               
               var quiz_detail_time = new Date();
               
               $.each(quizInfo.result, function(i, row) {
                      if(row.id == quizInfo.id) {
                      $('#quiz_data').append('<li>Name: '+row.quizName+'</li>');
                      $('#quiz_data').append('<li>Instructor : '+row.creator+'</li>');
                      $('#quiz_data').append('<li>Total Point : '+row.quizWorth+'</li>');
                      $('#quiz_data').append('<li>Due Day : '+row.dueDay+'</li>');
                      
                      //pull the score of the quiz if the quiz is completed
                      $.ajax({
                             type: 'GET',
                             url: port_location + 'api/group_b/studentQuiz/?quiz=' + quizInfo.id + key,
                             dataType: "json",
                             async: false,
                             success: function (data) {
                             console.log(data);
                             
                             //display score
                             if (data.length != 0){
                             if (data[0].completed === true || quiz_detail_time.toJSON() > data[0].quizDueTime){
                             $('#quiz_data').append('<li>Score : '+data[0].score+'</li>');
                             }
                             }
                             }
                             })
                      
                      $('#quiz_data').append('<li><a href="" data-id="' + row.id +'">Take This Quiz</a></li>');
                      $('#quiz_data').listview('refresh');
                      }
                      });
               });

//action of clicking a specific quiz in the quiz list
$(document).on('vclick', '#quiz_list li a', function(){
               quizInfo.id = $(this).attr('data-id');
               $.mobile.changePage( "#quiz_detail", { transition: "slide", changeHash: false });
               });

//action of taking a specific quiz
$(document).on('vclick', '#quiz_data li a', function(){
               quizInfo.id = $(this).attr('data-id');
               
               studentQuiz.quiz = quizInfo.id;
               //define the current time
               var now = new Date();
               
               //reset flag
               flag = false;
               resume = true;
               
               $.ajax({
                      url: port_location + 'api/group_b/studentQuiz/',
                      dataType: "json",
                      async: false,
                      success: function (data) {
                      studentQuizInfo.result = data;
                      },
                      error: function (request,error) {
                      alert('Cannot get studentQuiz. Network error has occurred please try again!');
                      }
                      })
               
               //check if quiz has been taken
               $.each(studentQuizInfo.result, function(i, item){
                      //if (item.quiz == studentQuiz.quiz){
                      if (item.quiz == studentQuiz.quiz){
                      //this quiz is already exist
                      flag = true;
                      
                      //this quiz is expired
                      if (item.completed){
                      resume = false;
                      }
                      //decide whether this quiz can be resumed or not
                      else if (now > item.quizDueTime){
                      resume = false;
                      }
                      }
                      });
               
               //if quiz is not yet taken, create a new one
               if (!flag){
               
               //when taking a specific quiz, reset all answer flag to true
               answer_flag_1 = true;
               answer_flag_2 = true;
               answer_flag_3 = true;
               answer_flag_4 = true;
               answer_flag_5 = true;
               
               //post a new studentQuiz to the server
               
               //set specific quiz
               studentQuiz.quiz = quizInfo.id;
               //*
               //set student to pk=1 for testing
               studentQuiz.student = student_id;
               //set dueTime to 30 mins after
               var dueTime = new Date();
               
               //*
               //dueTime.setMinutes(dueTime.getMinutes() + 20);
               dueTime.setSeconds(dueTime.getSeconds() + 30);
               
               due = dueTime;
               //2014-07-27T02:25:15
               dueTime = dueTime.getFullYear() + "-" + (dueTime.getMonth()+1) + "-" + (dueTime.getDay()+1) + "T" + dueTime.getHours() + ":" + dueTime.getMinutes() + ":" + dueTime.getSeconds();
               console.log(dueTime);
               
               studentQuiz.quizDueTime = dueTime;
               //set default score to 0
               studentQuiz.score = 0;
               
               //post studentQuiz
               $.ajax({
                      type: 'POST',
                      url: port_location + 'api/group_b/studentQuiz/' + key,
                      data: studentQuiz,
                      dataType: "json",
                      async: false,
                      success: function(data){
                      alert('Quiz begin, you have 30 minutes.');
                      
                      //set studentQuiz id to current quiz id
                      studentQuizInfo.id = data.id;
                      },
                      error: function(request,error){
                      alert('Cannot post studentQuiz.');
                      }
                      });
               
               //pull questions
               $.ajax({
                      url: port_location + 'api/group_b/question/?quiz=' + quizInfo.id + key,
                      dataType: "json",
                      async: true,
                      success: function (data) {
                      console.log(data);
                      question_ajax.parseJSONP(data);
                      $.mobile.changePage( "#quiz_content", { transition: "slide", changeHash: false });
                      },
                      error: function (request,error) {
                      alert('Cannot get questions. Network error has occurred please try again!');
                      }
                      });
               }
               //resume quiz
               else if (due != null && now < due && resume === true){
               console.log('resume quiz');
               alert('resume quiz');
               
               //pull questions
               $.ajax({
                      url: port_location + 'api/group_b/question/?quiz=' + quizInfo.id + key,
                      dataType: "json",
                      async: true,
                      success: function (data) {
                      console.log(data);
                      question_ajax.parseJSONP(data);
                      $.mobile.changePage( "#quiz_content", { transition: "slide", changeHash: false });
                      },
                      error: function (request,error) {
                      alert('Cannot get questions. Network error has occurred please try again!');
                      }
                      });
               }
               else{
               alert('Quiz is expired');
               }
               });

//parse a list of questions of a specific quiz
var question_ajax = {
parseJSONP:function(data){
    questionInfo.result = data;
    //clean up the quiz content
    $('#question_answer').empty();
    
    $.each(questionInfo.result, function(i, row){
           if (row.quiz == quizInfo.id){
           console.log(JSON.stringify(row));
           //pull possibleAnswers from backend
           GetPossibleAnswers(row.id, row.qText, row.qNumber);
           }
           });
    
    //make an ajax call when radio button is clicked
    //post student's answer to the backend
    
    //post answer to question #1
    $("input[name='answerSet1']").bind( "change", function(event, ui) {
                                       console.log($(this).val());
                                       
                                       studentAnswer.studentQuiz = studentQuizInfo.id;
                                       studentAnswer.answerText = $(this).val();
                                       var correctAnswer_1 = null;
                                       
                                       //select the correct answer, increment score
                                       for (i = 0; i < possibleAnswerInfo.result[0].length; i++){
                                       if (studentAnswer.answerText === possibleAnswerInfo.result[0][i].answerText && possibleAnswerInfo.result[0][i].correctAnswer === true && !score_increment_1){
                                            correctAnswer_1 = possibleAnswerInfo.result[0][i].answerText;
                                            studentQuiz.score += questionInfo.result[0].qWorth;
                                            score_increment_1 = true;
                                       
                                       }
                                       }
                                       
                                       //select the wrong answer and previous selection is correct, reduce score
                                       if (correctAnswer_1 != null && studentAnswer.answerText != correctAnswer_1){
                                            studentQuiz.score -= questionInfo.result[0].qWorth;
                                            score_increment_1 = false;
                                       }
                                       
                                       //student made a choice
                                       if (answer_flag_1){
                                       $.ajax({
                                              type: 'POST',
                                              url: port_location + 'api/group_b/studentAnswer/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been posted.');
                                              
                                              //get current studentAnswer id for the purpose of updating answer info
                                              studentAnswer_1_id = data.id;
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       
                                       //set answer_flag_1 to false: prevent duplicated POST for the same question
                                       answer_flag_1 = false;
                                       }
                                       
                                       //student change his/her choice
                                       else{
                                       $.ajax({
                                              type: 'PUT',
                                              url: port_location + 'api/group_b/studentAnswer/' + studentAnswer_1_id + '/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been changed.');
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       }
                                       });
    
    //post answer to question #2
    $("input[name='answerSet2']").bind( "change", function(event, ui) {
                                       console.log($(this).val());
                                       
                                       studentAnswer.studentQuiz = studentQuizInfo.id;
                                       studentAnswer.answerText = $(this).val();
                                       var correctAnswer_2 = null;
                                       
                                       //select the correct answer, increment score
                                       for (i = 0; i < possibleAnswerInfo.result[1].length; i++){
                                       if (studentAnswer.answerText === possibleAnswerInfo.result[1][i].answerText && possibleAnswerInfo.result[1][i].correctAnswer === true && !score_increment_2){
                                       correctAnswer_2 = possibleAnswerInfo.result[1][i].answerText;
                                       studentQuiz.score += questionInfo.result[1].qWorth;
                                       score_increment_2 = true;
                                       
                                       }
                                       }
                                       
                                       //select the wrong answer and previous selection is correct, reduce score
                                       if (correctAnswer_2 != null && studentAnswer.answerText != correctAnswer_2){
                                       studentQuiz.score -= questionInfo.result[1].qWorth;
                                       score_increment_2 = false;
                                       }
                                       
                                       //student made a choice
                                       if (answer_flag_2){
                                       $.ajax({
                                              type: 'POST',
                                              url: port_location + 'api/group_b/studentAnswer/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been posted.');
                                              
                                              //get current studentAnswer id for the purpose of updating answer info
                                              studentAnswer_2_id = data.id;
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       
                                       //set answer_flag_1 to false: prevent duplicated POST for the same question
                                       answer_flag_2 = false;
                                       }
                                       
                                       //student change his/her choice
                                       else{
                                       $.ajax({
                                              type: 'PUT',
                                              url: port_location + 'api/group_b/studentAnswer/' + studentAnswer_2_id + '/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been changed.');
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       }
                                       });
    
    //post answer to question #3
    $("input[name='answerSet3']").bind( "change", function(event, ui) {
                                       console.log($(this).val());
                                       
                                       studentAnswer.studentQuiz = studentQuizInfo.id;
                                       studentAnswer.answerText = $(this).val();
                                       var correctAnswer_3 = null;
                                       
                                       //select the correct answer, increment score
                                       for (i = 0; i < possibleAnswerInfo.result[2].length; i++){
                                       if (studentAnswer.answerText === possibleAnswerInfo.result[2][i].answerText && possibleAnswerInfo.result[2][i].correctAnswer === true && !score_increment_3){
                                       correctAnswer_3 = possibleAnswerInfo.result[2][i].answerText;
                                       studentQuiz.score += questionInfo.result[2].qWorth;
                                       score_increment_3 = true;
                                       
                                       }
                                       }
                                       
                                       //select the wrong answer and previous selection is correct, reduce score
                                       if (correctAnswer_3 != null && studentAnswer.answerText != correctAnswer_3){
                                       studentQuiz.score -= questionInfo.result[2].qWorth;
                                       score_increment_3 = false;
                                       }
                                       
                                       //student made a choice
                                       if (answer_flag_3){
                                       $.ajax({
                                              type: 'POST',
                                              url: port_location + 'api/group_b/studentAnswer/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been posted.');
                                              
                                              //get current studentAnswer id for the purpose of updating answer info
                                              studentAnswer_3_id = data.id;
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       
                                       //set answer_flag_1 to false: prevent duplicated POST for the same question
                                       answer_flag_3 = false;
                                       }
                                       
                                       //student change his/her choice
                                       else{
                                       $.ajax({
                                              type: 'PUT',
                                              url: port_location + 'api/group_b/studentAnswer/' + studentAnswer_3_id + '/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been changed.');
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       }
                                       });
    
    //post answer for question #4
    $("input[name='answerSet4']").bind( "change", function(event, ui) {
                                       console.log($(this).val());
                                       
                                       studentAnswer.studentQuiz = studentQuizInfo.id;
                                       studentAnswer.answerText = $(this).val();
                                       var correctAnswer_4 = null;
                                       
                                       //select the correct answer, increment score
                                       for (i = 0; i < possibleAnswerInfo.result[3].length; i++){
                                       if (studentAnswer.answerText === possibleAnswerInfo.result[3][i].answerText && possibleAnswerInfo.result[3][i].correctAnswer === true && !score_increment_4){
                                       correctAnswer_4 = possibleAnswerInfo.result[3][i].answerText;
                                       studentQuiz.score += questionInfo.result[3].qWorth;
                                       score_increment_4 = true;
                                       
                                       }
                                       }
                                       
                                       //select the wrong answer and previous selection is correct, reduce score
                                       if (correctAnswer_4 != null && studentAnswer.answerText != correctAnswer_4){
                                       studentQuiz.score -= questionInfo.result[3].qWorth;
                                       score_increment_4 = false;
                                       }
                                       
                                       //student made a choice
                                       if (answer_flag_4){
                                       $.ajax({
                                              type: 'POST',
                                              url: port_location + 'api/group_b/studentAnswer/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been posted.');
                                              
                                              //get current studentAnswer id for the purpose of updating answer info
                                              studentAnswer_4_id = data.id;
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       
                                       //set answer_flag_1 to false: prevent duplicated POST for the same question
                                       answer_flag_4 = false;
                                       }
                                       
                                       //student change his/her choice
                                       else{
                                       $.ajax({
                                              type: 'PUT',
                                              url: port_location + 'api/group_b/studentAnswer/' + studentAnswer_4_id + '/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been changed.');
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       }
                                       });
    
    //post answer for question #5
    $("input[name='answerSet5']").bind( "change", function(event, ui) {
                                       console.log($(this).val());
                                       
                                       studentAnswer.studentQuiz = studentQuizInfo.id;
                                       studentAnswer.answerText = $(this).val();
                                       var correctAnswer_5 = null;
                                       
                                       //select the correct answer, increment score
                                       for (i = 0; i < possibleAnswerInfo.result[4].length; i++){
                                       if (studentAnswer.answerText === possibleAnswerInfo.result[4][i].answerText && possibleAnswerInfo.result[4][i].correctAnswer === true && !score_increment_5){
                                       correctAnswer_5 = possibleAnswerInfo.result[4][i].answerText;
                                       studentQuiz.score += questionInfo.result[4].qWorth;
                                       score_increment_5 = true;
                                       
                                       }
                                       }
                                       
                                       //select the wrong answer and previous selection is correct, reduce score
                                       if (correctAnswer_5 != null && studentAnswer.answerText != correctAnswer_5){
                                       studentQuiz.score -= questionInfo.result[4].qWorth;
                                       score_increment_5 = false;
                                       }
                                       
                                       //student made a choice
                                       if (answer_flag_5){
                                       $.ajax({
                                              type: 'POST',
                                              url: port_location + 'api/group_b/studentAnswer/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been posted.');
                                              
                                              //get current studentAnswer id for the purpose of updating answer info
                                              studentAnswer_5_id = data.id;
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       
                                       //set answer_flag_1 to false: prevent duplicated POST for the same question
                                       answer_flag_5 = false;
                                       }
                                       
                                       //student change his/her choice
                                       else{
                                       $.ajax({
                                              type: 'PUT',
                                              url: port_location + 'api/group_b/studentAnswer/' + studentAnswer_5_id + '/' + key,
                                              dataType: "json",
                                              data: studentAnswer,
                                              async: false,
                                              success: function(data){
                                              console.log('studentAnswer has been changed.');
                                              },
                                              error: function(request,error){
                                              alert('Cannot post studentAnswer.');
                                              }
                                              });
                                       }
                                       });
}
}

function GetPossibleAnswers(question_id, question_text, question_number){
    
    //pull the possible answers for a specific question
    $.ajax({
           url: port_location + 'api/group_b/possibleAnswer/?format=json&question=' + question_id,
           dataType: "json",
           async: false,
           success: function (data) {
           console.log(data);
           $('#question_answer').append('<fieldset data-role="controlgroup"' + 'id="question' + question_number +'">');
           $('#question_answer').append('<legend><strong>Question '+ question_number + '. </strong>' + question_text + '</>');
           
           //store all possible answers
           if (!flag){
           possibleAnswerInfo.result.push(data);
           }
           
           $.each(data, function(i, row){
                  console.log(JSON.stringify(row));
                  $('#question_answer').append('<input type="radio" name="answerSet'+ question_number + '"' + 'id="radio-choice-'+ question_number +'-'+ (i+1) + '"' + 'value="'+ row.answerText +'"/>');
                  $('#question_answer').append('<label for="radio-choice-'+ question_number +'-'+ (i+1) + '">' + row.answerText + '</label>');
                  });
           $('#question_answer').append('</fieldset>');
           },
           
           error: function (request,error) {
           alert('Cannot get poosible answers. Network error has occurred please try again!');
           }
           });
}
