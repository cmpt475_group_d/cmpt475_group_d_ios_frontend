
//////////////////////////////////////////////////////////
// attendance.js                                        //
// Written by Maria Kaardal                             //
// Last updated July 24 2014                             //
//                                                      //
// This file contains the functionality realted to      //
// controlling the student view for checking in.        //
//////////////////////////////////////////////////////////

/*
 * This function contains all the functions in the file and is executed
 * on load of the HTML file.
 */

$(document).ready(function() { /*on load run this function*/
    
    console.log("attendance");

    var class_id = localStorage.getItem('current_course_id');
    var student_id = localStorage.getItem('id')
      , url_base = "http://candyman.biz";
    /*
     * Check localStore for lastCheckedin information and append to <p>
     * #checkinDate in div #attendanceLastStatus
     */
    var date_last_checked_in = window.localStorage.getItem(class_id+"attendance_last_checkedin"+student_id);
    if(date_last_checked_in != undefined && date_last_checked_in != '') {
        $('#checkinDate').text(date_last_checked_in);
    }
    
    /*
     * This function compares the 4 digit code with the one from the 
     * server and updates the student's attendance if it is correct.
     */
    $('#inputPIN').on('click', function(event) {
        event.preventDefault();
        var real_pin;

        // Request the pin from the server
        $.ajax({
            url: url_base + "/api/group_e/Attendance_Check/?format=json" + "&class_id=" + class_id,
            async: false,
            type: "GET",
            data: "",
            dataType: 'json',
            success: function(data)
            {
                if (data.length > 0) {
                  real_pin = parseInt(data[data.length - 1].code);
                }
            }
        });

        // get the inputed pin
        console.log(real_pin);
        var in_pin = parseInt($("#inputDATA").val());
        console.log(in_pin);
        if (in_pin === real_pin) {
            console.log("match");
            // Update the student if they have the right value.
            // Step 1: find out the id of the record.
            var record_id;
            var time;
            $.ajax({                                      
                url: url_base + "/api/group_e/Attendance/?format=json&student_id=" + student_id + "&class_id=" + class_id,
                async: false, 
                type: "GET",                   //the script to call to get data          
                data: "",                          //you can insert url argumnets here to pass to api.php
                                                                                 //for example "id=5&parent=6"
                dataType: 'json',                //data format      
                success: function(data)          //on recieve of reply
                {
                    if (data.length > 0) {
                        console.log(data);
                        record_id = data[data.length - 1].id;
                        time = data[data.length - 1].timestamp;

                        var dateStrSplit = time.split('Z')[0].split('T');
                        var dateArr = dateStrSplit[0].split('-');
                        var timeArr = dateStrSplit[1].split(':');

                        time = new Date(dateArr[0], dateArr[1] - 1, dateArr[2], timeArr[0], timeArr[1], timeArr[2], 0);
                    }
                }
            });
            // Step 2: Update the record
            $.ajax({                                      
                url: url_base + "/api/group_e/Attendance/" + record_id + "/",
                async: false,   
                type: "PATCH",                   //the script to call to get data          
                data: "status=1",                          //you can insert url argumnets here to pass to api.php
                                                                                 //for example "id=5&parent=6"
                dataType: 'json',                //data format      
                success: function(data)
                {
                    console.log("success!");
                    var text = $("#outputDate").text().split("$");
                    if (text.length == 2 && text[1] === "Date") {
                        text = text[0] + time.toUTCString();
                        $("#outputDate").text(text);  
                    }
                    
                    //Save checkin time to localStorage for later access (time last checked in)
                    var storage_time = $("#attendanceLastStatus").text().split(":");
                    storage_time = storage_time[0] + ": " + time.toUTCString();
                    window.localStorage.setItem(class_id+"attendance_last_checkedin"+student_id, storage_time);
                    
                    $("#attendanceWrongPin").hide();
                    $("#attendanceStatusLocked").hide();
                    $("#attendanceStatusUnlocked").hide();
                    $("#attendanceStatusCheckedIn").show();
                    $("#attendancePIN").hide();
                    $("#attendanceLastStatus").hide();
                }
            });
        }
        else {
            // They entered a wrong pin. Let them know.
            $("#attendanceWrongPin").show();
            $("#attendanceStatusLocked").hide();
            $("#attendanceStatusUnlocked").show();
            $("#attendanceStatusCheckedIn").hide();
            $("#attendancePIN").show();
            $("#attendanceLastStatus").show();
        }

        event.stopImmediatePropagation();
    });

    /*
     * This function checks the status of the current attendance session.
     * If it is open, and the student is not checked in, it allows the
     * the student to check in. If it is open and the student is checked
     * in it displays a message showing that the student is checked in. 
     * If it is closed it displays a message letting the student know
     * that attendance is locked.
     */
    $(function() {
        var status;
        var current_time;
        var open_time;
        var exp;

        // Request the status of the current attendance session.
        $.ajax({
            url: url_base + "/api/group_e/Attendance_Check/?format=json" + "&class_id=" + class_id,
            async: false,
            type: "GET",
            data: "",
            dataType: 'json',
            timeout: 3500,
            success: function(data)
            {
                if (data.length > 0) {
                    open_time = data[data.length - 1].timestamp;
                    exp = data[data.length - 1].expiration_time;
                    current_time = data[data.length - 1].call_time;
                }
                $("#AttendanceNoConnectivity").hide();
            },
            error: function(data)
            {
                $("#attendanceWrongPin").hide();
                $("#attendanceStatusLocked").hide();
                $("#attendanceStatusUnlocked").hide();
                $("#attendanceStatusCheckedIn").hide();
                $("#attendancePIN").hide();
                $("#attendanceLastStatus").show();
                $("#AttendanceNoConnectivity").show();
            }
        });

        // Convert the opening time to a javascript date
        var dateStrSplit = open_time.split('Z')[0].split('T');
        var dateArr = dateStrSplit[0].split('-');
        var timeArr = dateStrSplit[1].split(':');

        open_time = new Date(dateArr[0], dateArr[1] - 1, dateArr[2], timeArr[0], timeArr[1], timeArr[2], 0);
        
        // Convert the closing time to a javascript date
        var dateStrSplit2 = current_time.split('Z')[0].split('T');
        var dateArr2 = dateStrSplit2[0].split('-');
        var timeArr2 = dateStrSplit2[1].split(':');

        current_time = new Date(dateArr2[0], dateArr2[1] - 1, dateArr2[2], timeArr2[0], timeArr2[1], timeArr2[2], 0);
        
        // If the time is within exp minutes, check the student status and display accrodingly
        if (Math.abs(open_time - current_time) < exp*1000) {
            // request student status.
            $.ajax({                                      
                url: url_base + "/api/group_e/Attendance/?format=json" + "&student_id=" + student_id + "&class_id=" + class_id,
                async: false, 
                type: "GET",                   //the script to call to get data          
                data: "",                          //you can insert url argumnets here to pass to api.php
                                                                                 //for example "id=5&parent=6"
                dataType: 'json',                //data format      
                success: function(data)          //on recieve of reply
                {
                    if (data.length > 0) {
                        status = data[data.length - 1].status;
                    }
                }
            });

            // Display according to student status.
            if (status == "0") {
                $("#attendanceWrongPin").hide();
                $("#attendanceStatusLocked").hide();
                $("#attendanceStatusUnlocked").show();
                $("#attendanceStatusCheckedIn").hide();
                $("#attendancePIN").show();
                $("#attendanceLastStatus").show();
            } 
            else
            {
                // Add the date to the output
                var text = $("#outputDate").text().split("$");
                if (text.length == 2 && text[1] === "Date") {
                    text = text[0] + (open_time.toUTCString());
                    $("#outputDate").text(text); 
                }
                $("#attendanceWrongPin").hide();
                $("#attendanceStatusLocked").hide();
                $("#attendanceStatusUnlocked").hide();
                $("#attendanceStatusCheckedIn").show();
                $("#attendancePIN").hide();
                $("#attendanceLastStatus").hide();
            }
        }
        else
        {
            $("#attendanceWrongPin").hide();
            $("#attendanceStatusLocked").show();
            $("#attendanceStatusUnlocked").hide();
            $("#attendanceStatusCheckedIn").hide();
            $("#attendancePIN").hide();
            $("#attendanceLastStatus").show();
        }

    }); 
});