# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Team #1 iOS front-end team member
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Fork this repo
2. Test it out on iOS simulator
3. Please leave the config (.xml) file alone in sprint cycle one
4. Dependencies: jQuery, jQuery Mobile 

### Contribution guidelines ###

* Please follow the coding guideline (put your code in the right place)
* If you need assistance, feel free to email me: chenjienan2009@gmail.com 
* convention: e.g. group_d_material.html in src folder
* convention: e.g. group_b_navbar.css in css folder

### Coding Standard ###

Please follow the following coding style:

For CSS and HTML: https://google-styleguide.googlecode.com/svn/trunk/htmlcssguide.xml

For Javascript:
http://contribute.jquery.org/style-guide/js/
